use serde::{Deserialize, Serialize};
use chrono::{DateTime, Utc};
use std::str;
use std::convert::Infallible;


#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    name: String,
    email: String,
    username: Option<String>,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Commit {
    message: String,
    author: User,
    committer: User,
    url: String,
    timestamp: DateTime<Utc>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Repository {
    full_name: String
}


#[derive(Serialize, Deserialize, Debug)]
pub struct PushHook {
    repository: Repository,
    commits: Vec<Commit>,
}

pub async fn push(token: String, hook: PushHook) -> Result<impl warp::Reply, Infallible> {
    let PushHook {repository: Repository {full_name: projectname },commits} = hook;

    for commit in &commits {
        let Commit {message, author, url, timestamp, ..} = commit;
        let User { name: authorname, .. } = author;

        crate::sinks::send_message_to_project(&token,
                                                 format!("{} ({}) @ {}\n{}\n\n{}", authorname, projectname, timestamp, url, message)).await;
    }

    Ok("push")
}
