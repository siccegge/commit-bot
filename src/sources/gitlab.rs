use serde::{Deserialize, Serialize};
use chrono::{DateTime, Utc};
use std::str;
use std::convert::Infallible;

#[derive(Serialize, Deserialize, Debug)]
pub struct Project {
    name: String,
    namespace: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct User {
    name: String,
    email: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Commit {
    message: String,
    title: String,
    author: User,
    timestamp: DateTime<Utc>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PushHook {
    event_name: String,
    user_name: String,
    project: Project,
    commits: Vec<Commit>,
}

pub async fn push(token: String, hook: PushHook) -> Result<impl warp::Reply, Infallible> {
    let PushHook {project, commits,
                  event_name: _,
                  user_name: _} = hook;
    let Project {name: projectname, namespace:_ } = project;

    for commit in &commits {
        let Commit {message, author, timestamp,
                    title:_ } = commit;
        let User { name: authorname, email:_ } = author;

        crate::sinks::send_message_to_project(&token,
                                                 format!("{} ({}) @ {}\n\n{}", authorname, projectname, timestamp, message)).await;
    }

    Ok("push")
}
