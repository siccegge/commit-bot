pub mod telegram;
pub mod zulip;

use uuid::Uuid;


enum SourceId {
    Telegram(i64),
    Zulip(i64),
}

fn subscribe(db: &rusqlite::Connection, id: &SourceId, proj:i64) {
    let (ctype, cid) = match &id {
        SourceId::Telegram(id) => ("T", id),
        SourceId::Zulip(id) => ("Z", id),
    };
    
    db.execute("INSERT INTO subscription (chatid, chattype, projectid) VALUES (:cid, :ctype, :project)",
               rusqlite::named_params!{":cid": cid,":ctype": ctype,":project": proj}).unwrap();
}


fn unsubscribe(db: &rusqlite::Connection, id: &SourceId, proj:i64) {
    let (ctype, cid) = match &id {
        SourceId::Telegram(id) => ("T", id),
        SourceId::Zulip(id) => ("Z", id),
    };
    
    db.execute("DELETE FROM subscription WHERE chatid = :cid AND chattype = :ctype AND projectid = :project",
               rusqlite::named_params!{":cid": cid,":ctype": ctype,":project": proj}).unwrap();
}


pub async fn send_message_to_project(projecttoken: &String, message: String)  {
    let db = crate::database::open().unwrap();

    let chats:Vec<SourceId> = {
        let mut stmt = db.prepare("SELECT chatid, chattype FROM project, subscription \
                                   WHERE token = :token AND project.id = subscription.projectid").unwrap();
        let rows = stmt.query_and_then::<_,rusqlite::Error, _,_>(&[(":token", projecttoken)], |row| {
            let cid:i64 = row.get(0)?;
            let ctype:String = row.get(1)?;
            Ok(match ctype.as_str() {
                "Z" => SourceId::Zulip(cid),
                "T" => SourceId::Telegram(cid),
                _ => unimplemented!()
            })
        }).unwrap();
        rows.map(|x| x.unwrap()).collect()
    };

    for chat in chats {
        match chat {
            SourceId::Telegram(id) => {telegram::send_message(id, &message).await;}
            SourceId::Zulip(id) => {zulip::send_message(id, &message).await;}
        }
    }
}


fn handle_command(id: SourceId, prefix: &str, command: &str) -> Option<String> {
    if ! command.starts_with(prefix) {
        return None;
    }
    let command = &command[prefix.len()..];
    let mut split = str::split_whitespace(command);

    match split.next() {
        Some("echo") => {
            return Some(split.rfold("".to_string(), |acc, x| format!("{} {}", x, acc)));
        },
        Some("about") => {
            return Some("Find the code at https://gitlab.cs.fau.de/siccegge/chaac-bot/".into());
        },
        Some("help") => {
            return Some(format!("Supported Commands:\n\
                             {0}newproject gitlab [name]\n\
                             {0}subscribe uuid\n\
                             {0}unsubscribe uuid\n\
                             {0}info uuid\n\
                             {0}help\n\
                             {0}about", prefix));
        },
        Some("info") => {
            match split.next() {
                Some(uuid) => {
                    let db = crate::database::open().unwrap();
                    let mut stmt = db.prepare("SELECT id, projname FROM project WHERE token = ?").unwrap();
                    let (projid, projname) = stmt.query_row(rusqlite::params![uuid], |row| Ok((row.get::<_,i64>(0)?,
                                                                                               row.get::<_,String>(1)?))).unwrap();
                    stmt = db.prepare("SELECT chatname FROM chat, subscription WHERE projectid = ? AND chat.id = subscription.chatid").unwrap();
                    let chats = stmt.query_and_then(rusqlite::params![projid], |row| row.get::<_,String>(0)).unwrap();
                    let chatlist = chats.fold("".into(), |acc, x| format!(" - {}\n{}", x.unwrap(), acc));
                    return Some(format!("{}\nSubscribers:\n{}", projname, chatlist));
                },
                None => {
                    return Some("Synopsis: /info uuid".into());
                }
            }
        }
        Some("subscribe") => {
            match split.next() {
                Some(uuid) => {
                    let db = crate::database::open().unwrap();
                    let mut stmt = db.prepare("SELECT id FROM project WHERE token = ?").unwrap();
                    let projid:i64 = stmt.query_row(rusqlite::params![uuid], |row| row.get(0)).unwrap();
                    subscribe(&db, &id, projid);
                    return Some("OK".into());
                },
                None => {
                    return Some("Synopsis: /subscribe uuid".into());
                }
            }
        }
        Some("unsubscribe") => {
            match split.next() {
                Some(uuid) => {
                    let db = crate::database::open().unwrap();
                    let mut stmt = db.prepare("SELECT id FROM project WHERE token = ?").unwrap();
                    let projid:i64 = stmt.query_row(rusqlite::params![uuid], |row| row.get(0)).unwrap();
                    unsubscribe(&db, &id, projid);
                    return Some("OK".into());
                },
                None => {
                    return Some("Synopsis: /unsubscribe uuid".into());
                }
            }
        }
        Some("newproject") => {
            match split.next() {
                Some("gitlab") => {
                    let db = crate::database::open().unwrap();
                    let uuid = format!("{}", Uuid::new_v4().to_hyphenated());
                    let name = split.rfold("".to_string(), |acc, x| format!("{} {}", x, acc));
                    let mut stmt = db.prepare("INSERT INTO project (token, projname) VALUES (?, ?);").unwrap();
                    stmt.execute(rusqlite::params![uuid, name]).unwrap();
                    let projid:i64 = db.last_insert_rowid();

                    subscribe(&db, &id, projid);
                    return Some(format!("Created new gitlab project {name}\n\
                                             To use in gitlab, add webhook with\n\
                                             URL https://bot.cryptozoo.eu/gitlab and \n\
                                             secret token {token}\n\n\
                                             This chat has been subscribed to the token already. To enable/disable notifications in use\n\
                                             /subscribe {token}\n\
                                             /unsubscribe {token}",
                                            name = name,
                                            token = uuid));
                },
                Some("github") => {
                    let db = crate::database::open().unwrap();
                    let uuid = format!("{}", Uuid::new_v4().to_hyphenated());
                    let name = split.rfold("".to_string(), |acc, x| format!("{} {}", x, acc));
                    let mut stmt = db.prepare("INSERT INTO project (token, projname) VALUES (?, ?);").unwrap();
                    stmt.execute(rusqlite::params![uuid, name]).unwrap();
                    let projid:i64 = db.last_insert_rowid();

                    subscribe(&db, &id, projid);
                    return Some(format!("Created new github project {name}\n\
                                             To use in github, add webhook with\n\
                                             URL https://bot.cryptozoo.eu/github/{token}\n\n\
                                             This chat has been subscribed to the token already. To enable/disable notifications in use\n\
                                             /subscribe {token}\n\
                                             /unsubscribe {token}",
                                            name = name,
                                            token = uuid));
                },
                _ => {
                    return Some("Synopsis: /newproject (gitlab|github) [name]".into());
                }
            }
        },
        _ => {}
    };

    
    None
}
