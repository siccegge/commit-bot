mod types;

use hyper::{Client, Request, Method, Body};
use hyper_tls::HttpsConnector;
use types::{Webhook,WebhookMessage,ReplyMessage,SendMessage};
use log::info;
use super::{handle_command,SourceId};
use rusqlite::OptionalExtension;


pub async fn send_message(id: i64, message: &String)  {
    info!("sending message\"{}\" to id {}", message, id);
    let db = crate::database::open().unwrap();

    let resp = {
        let mut stmt = db.prepare("SELECT stream,topic,isstream FROM zulipchat WHERE id = :id").unwrap();
        stmt.query_row(rusqlite::named_params!{":id":id}, |row| {Ok((row.get::<_,i64>(0)?,
                                                                     row.get::<_,String>(1)?,
                                                                     row.get::<_,bool>(2)?))}).optional().unwrap()
    };

    let token:String = {
        let mut stmt = db.prepare("SELECT confvalue FROM config WHERE confkey = 'zuliptoken'").unwrap();
        stmt.query_row([], |row| row.get(0)).unwrap()
    };


    if let Some((stream,topic,streamtype)) = resp {
        let https = HttpsConnector::new();
        let client = Client::builder()
            .build::<_, hyper::Body>(https);

        let body = if streamtype {
            SendMessage {
                chattype: "stream".into(),
                content: message.clone(),
                to: stream,
                topic: Some(topic),
            }
        } else {
            SendMessage {
                chattype: "private".into(),
                content: message.clone(),
                to: stream,
                topic: None,
            }
        };

        let req = Request::builder()
            .method(Method::POST)
            .uri("https://chat.cryptozoo.eu/api/v1/messages")
            .header("content-type", "application/x-www-form-urlencoded")
            .header("Authorization", format!("Basic {}", token))
            .body(Body::from(serde_urlencoded::to_string(&body).unwrap())).unwrap();
        info!("{:?}", req);
        let resp = client.request(req).await.unwrap();
        if !resp.status().is_success() {
            let body = hyper::body::to_bytes(resp.into_body()).await;
            info!("{:?}", body)
        }
    }
}


fn streamid(db: &rusqlite::Connection, stream: i64, topic: &str) -> SourceId {
    let mut stmt = db.prepare("SELECT id FROM zulipchat WHERE stream = :stream AND topic = :topic").unwrap();
    let dbid = match stmt.query_row(rusqlite::named_params!{":stream":stream, ":topic":topic}, |row| row.get::<_,i64>(0)).optional().unwrap() {
        Some(chatid) => chatid,
        None => {
            let mut stmt = db.prepare("INSERT INTO zulipchat (stream, topic, isstream) VALUES (:stream, :topic, 1);").unwrap();
            stmt.execute(rusqlite::named_params!{":stream":stream, ":topic":topic}).unwrap();
            db.last_insert_rowid()
        }
    };
    SourceId::Zulip(dbid)
}


fn privid(db: &rusqlite::Connection, stream: i64) -> SourceId {
    let mut stmt = db.prepare("SELECT id FROM zulipchat WHERE stream = :stream AND topic = \"\"").unwrap();
    let dbid = match stmt.query_row(rusqlite::named_params!{":stream":stream}, |row| row.get::<_,i64>(0)).optional().unwrap() {
        Some(chatid) => chatid,
        None => {
            let mut stmt = db.prepare("INSERT INTO zulipchat (stream, topic, isstream) VALUES (:stream, \"\", 1);").unwrap();
            stmt.execute(rusqlite::named_params!{":stream":stream}).unwrap();
            db.last_insert_rowid()
        }
    };
    SourceId::Zulip(dbid)
}


pub async fn webhook(hook: Webhook) -> Result<impl warp::Reply, warp::Rejection> {
    info!("{:?}", hook);

    let Webhook {bot_email, bot_full_name, message} = hook;
    let WebhookMessage{content,stream_id,sender_id,subject} = message;

    let db = crate::database::open().unwrap();
    if let Some(stream_id) = stream_id {
        let sid = streamid(&db, stream_id, &subject);

        match handle_command(sid, &format!("@**{}** ", bot_full_name), &content) {
            Some(resp) => Ok(warp::reply::json(&ReplyMessage{content:resp})),
            None => unreachable!()
        }
    } else {
        let sid = privid(&db, sender_id);

        match handle_command(sid, "", &content) {
            Some(resp) => Ok(warp::reply::json(&ReplyMessage{content:resp})),
            None => unreachable!()
        }
    }
}
