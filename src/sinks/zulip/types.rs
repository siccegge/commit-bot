use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ReplyMessage {
    pub content: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookMessage {
    pub content: String,
    pub stream_id: Option<i64>,
    pub sender_id: i64,
    pub subject: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Webhook {
    pub bot_email: String,
    pub bot_full_name: String,
    pub message: WebhookMessage,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SendMessage {
    #[serde(rename = "type")]
    pub chattype: String,
    pub to: i64,
    pub content: String,
    pub topic: Option<String>,
}

