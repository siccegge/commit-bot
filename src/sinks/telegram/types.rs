use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct SendMessage {
    pub chat_id: i64,
    pub text: String,
    //            parse_mode: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookContent {
    pub text: String
}



#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookChat {
    pub title: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookUser {
    pub username: String,
    pub first_name: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookSource {
    pub id: i64,

    #[serde(rename = "type")]
    pub chattype: Option<String>,

    #[serde(flatten)]
    pub chat: Option<WebhookChat>,

    #[serde(flatten)]
    pub user: Option<WebhookUser>,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct WebhookMessage {
    pub from: WebhookSource,
    pub chat: WebhookSource,
    pub text: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Webhook {
    update_id: i64,
    pub message: WebhookMessage,
}

