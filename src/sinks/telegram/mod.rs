use hyper::{Client, Request, Method, Body};
use hyper_tls::HttpsConnector;
use std::convert::Infallible;
use std::str;

use rusqlite::OptionalExtension;


use log::info;

mod types;
use types::{SendMessage,Webhook,WebhookSource,WebhookMessage,WebhookChat,WebhookUser};

use crate::sinks::{handle_command,SourceId};


pub async fn send_message(id: i64, message: &String)  {
    info!("sending message\"{}\" to id {}", message, id);
    let db = crate::database::open().unwrap();

    let resp = {
        let mut stmt = db.prepare("SELECT chatid FROM telegramchat WHERE id = :id").unwrap();
        stmt.query_row(rusqlite::named_params!{":id":id}, |row| row.get::<_,i64>(0)).optional().unwrap()
    };

    if let Some(chatid) = resp {
        let https = HttpsConnector::new();
        let client = Client::builder()
            .build::<_, hyper::Body>(https);

        let token:String = {
            let mut stmt = db.prepare("SELECT confvalue FROM config WHERE confkey = 'telegramtoken'").unwrap();
            stmt.query_row([], |row| row.get(0)).unwrap()
        };

        let body = SendMessage {
            chat_id: chatid,
            text: message.clone(),
            //                parse_mode: "MarkdownV2".to_string()
        };
        let req = Request::builder()
            .method(Method::POST)
            .uri(format!("https://api.telegram.org/bot{}/sendMessage", token))
            .header("content-type", "application/json")
            .body(Body::from(serde_json::to_string(&body).unwrap())).unwrap();
        info!("{:?}", req);
        let resp = client.request(req).await.unwrap();
        if !resp.status().is_success() {
            let body = hyper::body::to_bytes(resp.into_body()).await;
            info!("{:?}", body)
        }
    }
}


pub async fn _webhook(bytes: bytes::Bytes) -> Result<impl warp::Reply, Infallible> {
    println!("{}", str::from_utf8(&bytes).unwrap());
    Ok("debug")
}


fn chatid(db: &rusqlite::Connection, chatid:i64, chatname:&str) -> SourceId {
    let mut stmt = db.prepare("SELECT id FROM telegramchat WHERE chatid = :id").unwrap();
    let dbid = match stmt.query_row(rusqlite::named_params!{":id":chatid}, |row| row.get::<_,i64>(0)).optional().unwrap() {
        Some(chatid) => chatid,
        None => {
            let mut stmt = db.prepare("INSERT INTO telegramchat (chatid, chatname) VALUES (:id, :name) RETURNING id").unwrap();
            stmt.query_row(rusqlite::named_params!{":id":chatid, ":name":chatname}, |row| row.get::<_,i64>(0)).unwrap()
        }
    };
    SourceId::Telegram(dbid)
}

pub async fn webhook(hook: Webhook) -> Result<impl warp::Reply, warp::Rejection> {
    info!("{:?}", hook);
    let Webhook { message, .. } = hook;
    let WebhookMessage { chat, text, from:_ } = message;
    let WebhookSource { id, chat, user, .. } = chat;

    let chatname:String = match user {
        None => {
            match chat {
                Some(WebhookChat { title, .. }) => title,
                None => "".into()
            }
        },
        Some(WebhookUser { username, .. }) => format!("@{}", username),
    };

    let db = crate::database::open().unwrap();
    let sid = chatid(&db, id, &chatname);

    let id = match sid {
        SourceId::Telegram(id) => id,
        _ => unreachable!()
    };

    match handle_command(sid, "/", &text) {
        Some(resp) => send_message(id, &resp).await,
        None => ()
    };
    Ok("ok")
}
