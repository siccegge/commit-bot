use rusqlite::{Connection,Result};

pub fn open () -> Result<Connection> {
    Connection::open("db/commitbot.sqlite")
}
