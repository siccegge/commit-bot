use warp::Filter;
use std::net::SocketAddr;
use std::convert::Infallible;
use std::str;

mod database;

mod sinks;
mod sources;

#[tokio::main]
pub async fn main()  {
    pretty_env_logger::init();

    let db = crate::database::open().unwrap();

    let token:String = {
        let mut stmt = db.prepare("SELECT confvalue from config where confkey = 'telegramtoken'").unwrap();
        stmt.query_row([], |row| row.get(0)).unwrap()
    };


    let log = warp::log("server::requests");

    let route_telegram = warp::path("telegram")
         .and(warp::path(token))
         .and(warp::body::json())
         .and_then(sinks::telegram::webhook);

    let route_zulip = warp::path("zulip")
         .and(warp::body::json())
         .and_then(sinks::zulip::webhook);
    
    let route_fallback = warp::path("debug")
        .and(warp::body::bytes())
        .and_then(fallback);

    let route_gitlab_push = warp::path("gitlab")
        .and(warp::header::<String>("X-Gitlab-Token"))
        .and(warp::body::json())
        .and_then(sources::gitlab::push);
    let route_gitlab = route_gitlab_push;

    let route_github_push = warp::path("github")
        .and(warp::path::param())
        .and(warp::body::json())
        .and_then(sources::github::push);
    let route_github = route_github_push;

    
    
    let routes = warp::post().and(route_gitlab.or(route_github).or(route_telegram).or(route_zulip).or(route_fallback));


    let v4addr:SocketAddr = "0.0.0.0:8443".parse().unwrap();
    let v4 = warp::serve(routes.clone().with(log.clone()))
        .tls()
        .cert_path("ssl/cert.pem")
        .key_path("ssl/key8.pem")
        .run(v4addr);
    let v6addr:SocketAddr = "[::]:8443".parse().unwrap();
    let v6 = warp::serve(routes.clone().with(log.clone()))
        .tls()
        .cert_path("ssl/cert.pem")
        .key_path("ssl/key8.pem")
        .run(v6addr);

    if cfg!(target_os = "linux") {
        futures::join!(v6);
    } else {
        futures::join!(v4, v6);
    }
}

pub async fn fallback(bytes: bytes::Bytes) -> Result<impl warp::Reply, Infallible> {
    println!("{}", str::from_utf8(&bytes).unwrap());
    Ok("fallback")
}
