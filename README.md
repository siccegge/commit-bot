# Building

it's just a rust project so `cargo build --release` should do the trick

# Running

Systemd Unit file
```
[Unit]
Description=Gitbot

[Service]
Type=simple
WorkingDirectory=/srv/gitbot/
ExecStart=/srv/gitbot/bin/gitlab-commit
User=_gitbot
Group=_gitbot
Environment=RUST_LOG=server::requests
```

nginx reverse proxy (for sharing port 443)
```
server {
        listen                  443 ssl http2;
        listen                  [::]:443 ssl http2;
        server_name             example.com;

        ssl_certificate         [...];
        ssl_certificate_key     [...];

        location / {
                proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header	    X-Forwarded-Proto https;
		        proxy_set_header        Host $http_host;
                proxy_pass              https://example.com:8443;
        }
}
```

You will have trouble running on ports other than 443
