CREATE TABLE IF NOT EXISTS "project" (
	id			INTEGER PRIMARY KEY,
	token		VARCHAR(128),
	projname	TEXT
);

CREATE TABLE IF NOT EXISTS "telegramchat" (
	id			INTEGER PRIMARY KEY,
	chatid		INTEGER,
	chatname	TEXT
);

CREATE TABLE IF NOT EXISTS "zulipchat" (
	id 		 	INTEGER PRIMARY KEY,
	zinstance	VARCHAR(128),
	stream		INTEGER,
	topic		TEXT,
	isstream	BOOLEAN
);

CREATE TABLE IF NOT EXISTS "subscription" (
	chatid		INTEGER,
	chattype	CHAR,
	projectid	INTEGER
);

CREATE TABLE IF NOT EXISTS "config" (
	confkey		TEXT,
	confvalue		TEXT
);
